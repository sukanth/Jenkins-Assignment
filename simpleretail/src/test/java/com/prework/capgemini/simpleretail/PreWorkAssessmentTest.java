package com.prework.capgemini.simpleretail;

/**
 * @author sugunda
 * @version 1.0
 */
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.prework.capgemini.simpleretail.Item.shippingMethod;

public class PreWorkAssessmentTest {
	private static SimpleRetailDriver simpleRetail = null;
	private static ProductCodeComparator codeComparator = null;

	
	@Before
	public void setUp() throws Exception {
		 simpleRetail = new SimpleRetailDriver();
		 codeComparator = new ProductCodeComparator();
	}
	
	@After
	public void garbageCollect() {
		System.gc();
	}

	@Test
	public void calShippingCostForAirTest() {
		double shippingCost = SimpleRetailDriver.calShippingCost(shippingMethod.AIR, 567321101985l,0.58f);
		assertEquals(shippingCost, 4.64f,0.001);
	}
	
	@Test
	public void calShippingCostForGroundTest() {
		double shippingCost = SimpleRetailDriver.calShippingCost(shippingMethod.GROUND, 567321101985l,0.58f);
		assertEquals(shippingCost, 1.45f, 0.001);
	}
	
	 @Test
	 public void calShippingCostForRailTest() {
		 double shippingCost = SimpleRetailDriver.calShippingCost(shippingMethod.RAIL, 312321101516l, 793.41f);
		 assertEquals(10, shippingCost,0.001);
	 }
	
	 @Test
	    public void itemObjectEqualTest() {
	        Item o1 = new Item(567321101987l, "CD � Pink Floyd, Dark Side Of The Moon", 19.99f, 0.58f, shippingMethod.AIR);
	        Item o2 = new Item(567321101987l, "CD � Pink Floyd, Dark Side Of The Moon", 19.99f, 0.58f, shippingMethod.AIR);
	        int result = codeComparator.compare(o1, o2);
	        assertTrue("Both Objects are Equal", result == 0);
	        
	    }
	 
	 @Test
	 public void itemObjectGreaterTest() {
		 Item o1 = new Item(567321101985l, "CD � Queen, A Night at the Opera", 20.49f, 0.55f, shippingMethod.AIR);
		 Item o2 = new Item(567321101984l, "CD � Michael Jackson, Thriller", 23.88f, 0.50f, shippingMethod.GROUND);
		 int result = codeComparator.compare(o1, o2);
		 assertTrue("Item o1 is greater than Item o2", result >= 1);
		 
	 }
	 
	 @Test
	 public void itemObjectLesserTest() {
		 Item o1 = new Item(567321101984l, "CD � Michael Jackson, Thriller", 23.88f, 0.50f, shippingMethod.GROUND);
		 Item o2 = new Item(567321101985l, "CD � Queen, A Night at the Opera", 20.49f, 0.55f, shippingMethod.AIR); 
		 int result = codeComparator.compare(o1, o2);
		 assertTrue("Item o2 is greater than Item o1", result <= 1);
	 }
}
