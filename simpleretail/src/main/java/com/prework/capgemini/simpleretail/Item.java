package com.prework.capgemini.simpleretail;

/**
 * 
 * @author Sukanth Gunda
 * @version 1.0
 * File Name : Item.java
 */

public class Item{

	private long pCode; // datatype to hold product code
	private String pDescription; // datatype to hold Product Description
	private float pPrice; // datatype to hold product price
	private float pWeight; // datatype to hold product weight
	private shippingMethod pShippingMethod; // datatype to hold shipping method

	public enum shippingMethod {
		AIR,GROUND,RAIL
	}

	/*Parameterized constructor with fields and calculation to parse the float value to 2 digits*/
	public Item(long pCode, String pDescription, float pPrice, float pWeight, shippingMethod pShippingMethod) {
		super();
		this.pCode = pCode;
		this.pDescription = pDescription;
		this.pPrice = Float.parseFloat(String.format("%.2f", pPrice));
		this.pWeight = Float.parseFloat(String.format("%.2f", pWeight));;
		this.pShippingMethod = pShippingMethod;
	}
	
	

	public long getpCode() {
		return pCode;
	}

	public void setpCode(long pCode) {
		this.pCode = pCode;
	}

	public String getpDescription() {
		return pDescription;
	}

	public void setpDescription(String pDescription) {
		this.pDescription = pDescription;
	}

	public float getpPrice() {
		return pPrice;
	}

	public void setpPrice(float pPrice) {
		this.pPrice = pPrice;
	}

	public float getpWeight() {
		return pWeight;
	}

	public void setpWeight(float pWeight) {
		this.pWeight = pWeight;
	}

	public shippingMethod getpShippingMethod() {
		return pShippingMethod;
	}

	public void setpShippingMethod(shippingMethod pShippingMethod) {
		this.pShippingMethod = pShippingMethod;
	}
}
