package com.prework.capgemini.simpleretail;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;


import com.prework.capgemini.simpleretail.Item.shippingMethod;


/**
 * 
 * @author Sukanth Gunda
 * @version 1.0
 * File Name : SimpleRetailDriver.java
 */
public class SimpleRetailDriver extends Utility{
	private static Item item;
	private static double shippingTarrif;
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		
		/*Array List to hold the items data in a list*/
		ArrayList<Item> items = new ArrayList<Item>();
		
		items.add(new Item(567321101987l, "CD � Pink Floyd, Dark Side Of The Moon", 19.99f, 0.58f, shippingMethod.AIR));
		items.add(new Item(567321101986l, "CD � Beatles, Abbey Road", 17.99f, 0.61f, shippingMethod.GROUND));
		items.add(new Item(567321101985l, "CD � Queen, A Night at the Opera", 20.49f, 0.55f, shippingMethod.AIR));
		items.add(new Item(567321101984l, "CD � Michael Jackson, Thriller", 23.88f, 0.50f, shippingMethod.GROUND));
		items.add(new Item(467321101899l, "iPhone - Waterproof Case", 9.75f, 0.73f, shippingMethod.AIR));
		items.add(new Item(477321101878l, "iPhone -  Headphones", 17.25f, 3.21f, shippingMethod.GROUND));
		items.add(new Item(312321101516l, "Hot Tub", 9899.99f, 793.41f, shippingMethod.RAIL));
		items.add(new Item(322322202488l, "HeavyMac Laptop", 4555.79f, 4.08f, shippingMethod.RAIL));
		
		
		/*Sorting the list using Comparator which in turn overrides the hash code and equals method of the object class*/
		Collections.sort(items, new ProductCodeComparator());
		System.out.println(String.format("%-40s %s", "****** Shipment report****",new Date()));
		System.out.println("");
		System.out.format("%-15s %-40s %-10s %-10s %-15s %-15s %n", "UPC","Description","Price","Weight","Ship Method","Shipping Cost");
		for(Item item : items) { 
			System.out.format("%-15s %-40s %-10s %-10s %-15s %-15s %n", item.getpCode(),item.getpDescription(),formatter.format(item.getpPrice()),item.getpWeight(),item.getpShippingMethod(),formatter.format(calShippingCost(item.getpShippingMethod(), item.getpCode(), item.getpWeight())));
			shippingTarrif = shippingTarrif + calShippingCost(item.getpShippingMethod(), item.getpCode(), item.getpWeight());
		}
		System.out.println("");
		System.out.println("TOTAL SHIPPING COST :");
		System.out.format("%-94s %-1s"," ",formatter.format(shippingTarrif));
	}
}
